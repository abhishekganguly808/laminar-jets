%% Initiate
clear
a=zeros(481,361);
b=zeros(481,361);
t1=zeros(481,361);
t2=zeros(481,361);
t3=zeros(481,361);
t4=zeros(481,361);
t5=zeros(481,361);

%% process
d=1;
L=80*d/2;
H=40*d/2;
l1=20*d/2;
h_heat=2*d/2;
gap=1;  %%gap/h_heat
i_n=7;
beta=1.5;
i_tmax=20*(i_n-1)+1; %n(grid-1)+1
i_max=80*(i_n-1)+1;
j_max=(80/2)*(i_n-1)+1;
dx=L/(i_max-1);
dy=(1/beta)*dx;
j_maxn=20/dy+1;
j_tmaxn=j_maxn-1-2*(i_n-1)*beta+1;
j_nn=(H-(gap+1))/dy+1;
x=linspace(0,L,i_max);
y=linspace(0,H,j_maxn);
u=a';
v=b';
T1=t1';
T2=t2';
T3=t3';
T4=t4';
T5=t5';
for k=1:2:9
    y_new=(19-y);
    u_ext=u(:,k/dx+1);
    v_ext=v(:,k/dx+1);
    T1_ext=T1(:,k/dx+1);
    T5_ext=T5(:,k/dx+1);
    mag=max(u_ext);
    mag_v=max(v_ext);
    u_ND=u_ext/mag;
    v_ND=v_ext/mag_v;
    pos_u=find(u_ext==mag);
    pos_v=find(v_ext==mag_v);
    pos_t1=j_tmaxn;
    pos_t5=j_tmaxn;  
    for j=pos_u:-1:1
        if u_ext(j)<0.5*mag
            lock=j+1;
            break;
        else
        end
    end
    for j=pos_v:-1:1
        if v_ext(j)<0.5*mag_v
            lock_v=j+1;
            break;
        else
        end
    end
    for j=pos_t1:-1:1
        if T1_ext(j)<0.5
            lock_t1=j+1;
            break;
        else
        end
    end
    for j=pos_t5:-1:1
        if T5_ext(j)<0.5
            lock_t5=j+1;
            break;
        else
        end
    end
    y_del=((y_new(lock)-y_new(lock-1))/(u_ext(lock)-u_ext(lock-1)))*(0.01*mag-u_ext(lock-1))+y_new(lock-1);
    y_del_v=((y_new(lock_v)-y_new(lock_v-1))/(v_ext(lock_v)-v_ext(lock_v-1)))*(0.01*mag_v-v_ext(lock_v-1))+y_new(lock_v-1);
    y_del_t1=((y_new(lock_t1)-y_new(lock_t1-1))/(T1_ext(lock_t1)-T1_ext(lock_t1-1)))*(0.01-T1_ext(lock_t1-1))+y_new(lock_t1-1);
    y_del_t5=((y_new(lock_t5)-y_new(lock_t5-1))/(T5_ext(lock_t5)-T5_ext(lock_t5-1)))*(0.01-T5_ext(lock_t5-1))+y_new(lock_t5-1);
    y_ND=y_new/y_del;
    y_ND_v=y_new/y_del_v;
    y_ND_t1=y_new/y_del_t1;
    y_ND_t5=y_new/y_del_t5;
    if k==1
        u_ND1=u_ND;
        y_ND1=y_ND;
        v_ND1=v_ND;
        y_ND1_v=y_ND_v;
        y_ND1_t1=y_ND_t1;
        y_ND1_t5=y_ND_t5;
        T1_ND1=T1_ext;
        T5_ND1=T5_ext;
    elseif k==3
         u_ND3=u_ND;
        y_ND3=y_ND;
        v_ND3=v_ND;
        y_ND3_v=y_ND_v;
         y_ND3_t1=y_ND_t1;
        y_ND3_t5=y_ND_t5;
        T1_ND3=T1_ext;
        T5_ND3=T5_ext;
    elseif k==5
         u_ND5=u_ND;
        y_ND5=y_ND;
        v_ND5=v_ND;
        y_ND5_v=y_ND_v;
         y_ND5_t1=y_ND_t1;
        y_ND5_t5=y_ND_t5;
        T1_ND5=T1_ext;
        T5_ND5=T5_ext;
    elseif k==7
         u_ND7=u_ND;
        y_ND7=y_ND;
        v_ND7=v_ND;
        y_ND7_v=y_ND_v;
         y_ND7_t1=y_ND_t1;
        y_ND7_t5=y_ND_t5;
        T1_ND7=T1_ext;
        T5_ND7=T5_ext;
    else
         u_ND9=u_ND;
        y_ND9=y_ND;
        v_ND9=v_ND;
        y_ND9_v=y_ND_v;
         y_ND9_t1=y_ND_t1;
        y_ND9_t5=y_ND_t5;
        T1_ND9=T1_ext;
        T5_ND9=T5_ext;
    end
        
end
P=[15,17,19,22,25,30];
for i=1:6
    k=P(i);
    y_new=(20-y);
    u_ext=u(:,k/dx+1);
    v_ext=v(:,k/dx+1);
    T1_ext=T1(:,k/dx+1);
    T5_ext=T5(:,k/dx+1);
    mag=max(u_ext);
    mag_v=max(v_ext);
    mag_t1=max(T1_ext);
    mag_t5=max(T5_ext);
    u_ND=u_ext/mag;
    v_ND=v_ext/mag_v;
    pos_u=find(u_ext==mag);
    pos_v=find(v_ext==mag_v);
    pos_t1=find(T1_ext==mag_t1);
    pos_t5=find(T5_ext==mag_t5);
    
    for j=pos_u:-1:1
        if u_ext(j)<0.5*mag
            lock=j+1;
            break;
        else
        end
    end
    for j=pos_v:-1:1
        if v_ext(j)<0.5*mag_v
            lock_v=j+1;
            break;
        else
        end
    end
    for j=pos_t1:-1:1
        if T1_ext(j)<0.5*mag_t1
            lock_t1=j+1;
            break;
        else
        end
    end
    for j=pos_t5:-1:1
        if T5_ext(j)<0.5*mag_t5
            lock_t5=j+1;
            break;
        else
        end
    end
    y_del=((y_new(lock)-y_new(lock-1))/(u_ext(lock)-u_ext(lock-1)))*(0.01*mag-u_ext(lock-1))+y_new(lock-1);
    y_ND=y_new/y_del;
    y_del_v=((y_new(lock_v)-y_new(lock_v-1))/(v_ext(lock_v)-v_ext(lock_v-1)))*(0.01*mag_v-v_ext(lock_v-1))+y_new(lock_v-1);
    y_ND_v=y_new/y_del_v;
    y_del_t1=((y_new(lock_t1)-y_new(lock_t1-1))/(T1_ext(lock_t1)-T1_ext(lock_t1-1)))*(0.01*mag_t1-T1_ext(lock_t1-1))+y_new(lock_t1-1);
    y_del_t5=((y_new(lock_t5)-y_new(lock_t5-1))/(T5_ext(lock_t5)-T5_ext(lock_t5-1)))*(0.01*mag_t5-T5_ext(lock_t5-1))+y_new(lock_t5-1);
    y_ND_t1=y_new/y_del_t1;
    y_ND_t5=y_new/y_del_t5;
    if i==1
        u_ND15=u_ND;
        y_ND15=y_ND;
        v_ND15=v_ND;
        y_ND15_v=y_ND_v;
        y_ND15_t1=y_ND_t1;
        y_ND15_t5=y_ND_t5;
        T1_ND15=T1_ext;
        T5_ND15=T5_ext;
    elseif i==2
        u_ND17=u_ND;
        y_ND17=y_ND;
        v_ND17=v_ND;
        y_ND17_v=y_ND_v;
        y_ND17_t1=y_ND_t1;
        y_ND17_t5=y_ND_t5;
        T1_ND17=T1_ext;
        T5_ND17=T5_ext;
    elseif i==3
        u_ND19=u_ND;
        y_ND19=y_ND;
        v_ND19=v_ND;
        y_ND19_v=y_ND_v;
        y_ND19_t1=y_ND_t1;
        y_ND19_t5=y_ND_t5;
        T1_ND19=T1_ext;
        T5_ND19=T5_ext;
    elseif i==4
        u_ND22=u_ND;
        y_ND22=y_ND;
        v_ND22=v_ND;
        y_ND22_v=y_ND_v;
        y_ND22_t1=y_ND_t1;
        y_ND22_t5=y_ND_t5;
        T1_ND22=T1_ext;
        T5_ND22=T5_ext;
    elseif i==5
        u_ND25=u_ND;
        y_ND25=y_ND;
        v_ND25=v_ND;
        y_ND25_v=y_ND_v;
        y_ND25_t1=y_ND_t1;
        y_ND25_t5=y_ND_t5;
        T1_ND25=T1_ext;
        T5_ND25=T5_ext;
    else
        u_ND30=u_ND;
        y_ND30=y_ND;
        v_ND30=v_ND;
        y_ND30_v=y_ND_v;
        y_ND30_t1=y_ND_t1;
        y_ND30_t5=y_ND_t5;
        T1_ND30=T1_ext;
        T5_ND30=T5_ext;
    end
        
end

Nu_HS_1=zeros(1,j_maxn-j_tmaxn+1);
Nu_HS_2=zeros(1,j_maxn-j_tmaxn+1);
Nu_HS_3=zeros(1,j_maxn-j_tmaxn+1);
Nu_HS_4=zeros(1,j_maxn-j_tmaxn+1);
Nu_HS_5=zeros(1,j_maxn-j_tmaxn+1);
% Nu_wall_1=zeros(1,i_max-i_tmax+1);
% Nu_wall_5=zeros(1,i_max-i_tmax+1);
for i=1:j_maxn-j_tmaxn+1
    Nu_HS_1(i)=(T1(i+j_tmaxn-1,i_tmax)-T1(i+j_tmaxn-1,i_tmax+1))/(dx);
    Nu_HS_2(i)=(T2(i+j_tmaxn-1,i_tmax)-T2(i+j_tmaxn-1,i_tmax+1))/(dx);
    Nu_HS_3(i)=(T3(i+j_tmaxn-1,i_tmax)-T3(i+j_tmaxn-1,i_tmax+1))/(dx);
    Nu_HS_4(i)=(T4(i+j_tmaxn-1,i_tmax)-T4(i+j_tmaxn-1,i_tmax+1))/(dx);
    Nu_HS_5(i)=(T5(i+j_tmaxn-1,i_tmax)-T5(i+j_tmaxn-1,i_tmax+1))/(dx);

    %Nu_HS_1(i)=-(-3*T1(i+j_tmaxn-1,i_tmax)+4*T1(i+j_tmaxn-1,i_tmax+1)-T1(i+j_tmaxn-1,i_tmax+2))/(2*dx);
    %Nu_HS_5(i)=-(-3*T5(i+j_tmaxn-1,i_tmax)+4*T5(i+j_tmaxn-1,i_tmax+1)-T5(i+j_tmaxn-1,i_tmax+2))/(2*dx);
end
% for i=1:i_max-i_tmax+1
%     Nu_wall_1(i)=-(-3*T1(j_maxn,i+i_tmax-1)+4*T1(j_maxn-1,i+i_tmax-1)-T1(j_maxn-2,i+i_tmax-1))/(2*dy);
%     Nu_wall_5(i)=-(-3*T5(j_maxn,i+i_tmax-1)+4*T5(j_maxn-1,i+i_tmax-1)-T5(j_maxn-2,i+i_tmax-1))/(2*dy);
% end
Nu_H_1=zeros(1,i_tmax-1);
Nu_H_2=zeros(1,i_tmax-1);
Nu_H_3=zeros(1,i_tmax-1);
Nu_H_4=zeros(1,i_tmax-1);
Nu_H_5=zeros(1,i_tmax-1);

for i=1:i_tmax
        Nu_H_1(i)=(T1(j_tmaxn,i)-T1(j_tmaxn-1,i))/(dy);
         Nu_H_2(i)=(T2(j_tmaxn,i)-T2(j_tmaxn-1,i))/(dy);
          Nu_H_3(i)=(T3(j_tmaxn,i)-T3(j_tmaxn-1,i))/(dy);
           Nu_H_4(i)=(T4(j_tmaxn,i)-T4(j_tmaxn-1,i))/(dy);
    Nu_H_5(i)=(T5(j_tmaxn,i)-T5(j_tmaxn-1,i))/(dy);

    %Nu_H_1(i)=-(-3*T1(j_tmaxn,i)+4*T1(j_tmaxn-1,i)-T1(j_tmaxn-2,i))/(2*dy);
    %Nu_H_5(i)=-(-3*T5(j_tmaxn,i)+4*T5(j_tmaxn-1,i)-T5(j_tmaxn-2,i))/(2*dy);
end
sum1=0;
sum5=0;
for i=2:length(Nu_HS_1)-1
    
    sum1=sum1+Nu_HS_1(i);
    sum5=sum5+Nu_HS_5(i);

end
Nu_HS_1_avg=(dy/1)*(sum1+Nu_HS_1(1)*0.5+Nu_HS_1(end)*0.5);
Nu_HS_5_avg=(dy/1)*(sum5+Nu_HS_5(1)*0.5+Nu_HS_5(end)*0.5);
sum1=0;
sum5=0;
for i=2:length(Nu_H_1)-1
    
    sum1=sum1+Nu_H_1(i);
    sum5=sum5+Nu_H_5(i);

 
end
Nu_H_1_avg=(dx/10)*(sum1+Nu_H_1(1)*0.5+Nu_H_1(end)*0.5);
Nu_H_5_avg=(dx/10)*(sum5+Nu_H_5(1)*0.5+Nu_H_5(end)*0.5);





figure(1)
subplot(2,2,1)
clear fig
plot(u_ND15,y_ND15)
hold on
axis square
axis ([0 1 0 1])
plot(u_ND17,y_ND17)
hold on
plot(u_ND19,y_ND19)
hold on
plot(u_ND22,y_ND22)
hold on
plot(u_ND25,y_ND25)
hold on
plot(u_ND30,y_ND30)
shg
subplot(2,2,2)
clear fig
plot(T1_ND15,y_ND15_t1)
hold on
axis square
axis ([0 1 0 1])
plot(T1_ND17,y_ND17_t1)
hold on
plot(T1_ND19,y_ND19_t1)
hold on
plot(T1_ND22,y_ND22_t1)
hold on
plot(T1_ND25,y_ND25_t1)
hold on
plot(T1_ND30,y_ND30_t1)
subplot(2,2,3)
clear fig
plot(T5_ND15,y_ND15_t5)
hold on
axis square
axis ([0 1 0 1])
plot(T5_ND17,y_ND17_t5)
hold on
plot(T5_ND19,y_ND19_t5)
hold on
plot(T5_ND22,y_ND22_t5)
hold on
plot(T5_ND25,y_ND25_t5)
hold on
plot(T5_ND30,y_ND30_t5)
subplot(2,2,4)
clear fig
plot(v_ND15,y_ND15_v)
hold on
axis square
axis ([0 1 0 1])
plot(v_ND17,y_ND17_v)
hold on
plot(v_ND19,y_ND19_v)
hold on
plot(v_ND22,y_ND22_v)
hold on
plot(v_ND25,y_ND25_v)
hold on
plot(v_ND30,y_ND30_v)




figure(2)
subplot(1,3,1)
clear fig
plot(u_ND1,y_ND1)
hold on
axis square
axis ([0 1 0 1])
plot(u_ND3,y_ND3)
hold on
plot(u_ND5,y_ND5)
hold on
plot(u_ND7,y_ND7)
hold on
plot(u_ND9,y_ND9)

subplot(1,3,2)
clear  fig
plot(T1_ND1,y_ND1_t1)
hold on
axis square
axis ([0 1 0 1])
plot(T1_ND3,y_ND3_t1)
hold on
plot(T1_ND5,y_ND5_t1)
hold on
plot(T1_ND7,y_ND7_t1)
hold on
plot(T1_ND9,y_ND9_t1)

subplot(1,3,3)
clear fig
plot(T5_ND1,y_ND1_t5)
hold on
axis square
axis ([0 1 0 1])
plot(T5_ND3,y_ND3_t5)
hold on
plot(T5_ND5,y_ND5_t5)
hold on
plot(T5_ND7,y_ND7_t5)
hold on
plot(T5_ND9,y_ND9_t5)
figure(3)
clear fig
plot(Nu_HS_1,20-y(j_tmaxn:j_maxn))
hold on
plot(Nu_HS_5,20-y(j_tmaxn:j_maxn))
%% Writer
%U
% fid=fopen('U1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U3.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND3(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U5.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U7.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND7(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND7(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U9.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND9(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND9(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U15.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND15(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND15(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U17.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND17(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND17(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U19.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND19(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND19(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U22.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND22(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND22(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U25.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND25(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND25(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('U30.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND30(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND30(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
%Jx1
% fid=fopen('Jx1_1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_3.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND3(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_5.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_7.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND7(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND7(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_9.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND9(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND9(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_15.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND15(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND15(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_17.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND17(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND17(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_19.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND19(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND19(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_22.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND22(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND22(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_25.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND25(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND25(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx1_30.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',u_ND30(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND30(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% % Jx5
% fclose(fid);
% fid=fopen('Jx5_1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND1_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_3.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND3_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_5.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND5_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_7.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND7(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND7_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_9.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND9(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND9_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_15.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND15(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND15_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_17.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND17(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND17_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_19.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND19(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND19_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_22.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND22(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND22_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_25.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND25(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND25_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Jx5_30.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(v_ext)
%         fprintf(fid, '%f ',v_ND30(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(v_ext)   
%         fprintf(fid, '%f ',y_ND30_v(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
%T1
% fid=fopen('T1_1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND1_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_3.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND3_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_5.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND5_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_7.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND7(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND7_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_9.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND9(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND9_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_15.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND15(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND15_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_17.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND17(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND17_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_19.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND19(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND19_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% 
% fid=fopen('T1_22.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND22(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND22_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_25.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND25(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND25_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T1_30.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T1" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T1_ND30(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND30_t1(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% 
% %T5
% fid=fopen('T5_1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND1_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_3.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND3_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_5.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND5_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_7.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND7(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND7_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_9.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND9(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND9_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_15.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND15(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND15_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_17.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND17(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND17_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_19.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND19(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND19_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_22.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND22(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND22_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_25.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND25(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND25_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('T5_30.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T5" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:length(u_ext)
%         fprintf(fid, '%f ',T5_ND30(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:length(u_ext)   
%         fprintf(fid, '%f ',y_ND30_t5(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% % Side wall Nu

fid=fopen('NuSideWallRe50G1.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "NU_HS_1" "NU_HS_2" "NU_HS_3" "NU_HS_4" "NU_HS_5" "Y" "\n');
fprintf(fid, 'ZONE I=19 J=1  K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_HS_1(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_HS_2(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_HS_3(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_HS_4(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_HS_5(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=j_tmaxn:j_maxn   
        fprintf(fid, '%f ',20-y(j));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
fclose(fid);
% fid=fopen('NuTopWallRe100G1.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "X" "NU_H_1" "NU_H_2" "NU_H_3" "NU_H_4" "NU_H_5"  "\n');
% fprintf(fid, 'ZONE I=1 J=121  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:i_tmax   
%         fprintf(fid, '%f ',x(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% for j=1:i_tmax
%         fprintf(fid, '%f ',Nu_H_1(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:i_tmax
%         fprintf(fid, '%f ',Nu_H_2(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:i_tmax
%         fprintf(fid, '%f ',Nu_H_3(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
%  for j=1:i_tmax
%         fprintf(fid, '%f ',Nu_H_4(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% for j=1:i_tmax
%         fprintf(fid, '%f ',Nu_H_5(j));      
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     fprintf(fid, '\n');
% end
% 
% fclose(fid);

function norm=normfind(a,b,starti,imax,startj,jmax)
  sum=0;
  for i=starti:imax
    for j=startj:jmax
        sum=sum+(a(j,i)-b(j,i))^2;
    end
  end
norm=sqrt(sum);
%norm=max(max(abs(a(startj:jmax,starti:imax)-b(startj:jmax,starti:imax))));
disp(norm);
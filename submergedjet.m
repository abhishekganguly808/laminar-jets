%% Domain
clc
clear
d=1;
L=80*d/2;
H=40*d/2;
l1=20*d/2;
h_heat=2*d/2;
gap=1;  %%gap/h_heat
i_n=7;
beta=1.5;
i_tmax=20*(i_n-1)+1; %n(grid-1)+1
i_max=80*(i_n-1)+1;
j_max=(80/2)*(i_n-1)+1;
dx=L/(i_max-1);
dy=(1/beta)*dx;
j_maxn=20/dy+1;
j_tmaxn=j_maxn-1-2*(i_n-1)*beta+1;
j_nn=(H-(gap+1))/dy+1;
x=linspace(0,L,i_max);
y=linspace(0,H,j_maxn);
nz_w=dx;
nz_dia=(i_n-1)*2*dx;
[x3d,y3d]=meshgrid(x,y);
distance=(gap+1)*h_heat;
nhfrac=dy*(j_nn-1)/H;
dt=0.5;
bw=1;
bt=1;
bp=1;
disp((j_tmaxn-j_nn)*dy)
disp((j_maxn-j_tmaxn)*dy)


%% Initialize
u=zeros(j_maxn,i_max);
v=zeros(j_maxn,i_max);
u_p=zeros(j_maxn,i_max);
v_p=zeros(j_maxn,i_max);
psy=zeros(j_maxn,i_max);
w=zeros(j_maxn,i_max);
Recx=zeros(j_maxn,i_max);
Recy=zeros(j_maxn,i_max);

Re=250;
Ri=0;
Gr=Ri*Re^2;
Pr=[0.71,2,4,6,7.56];
iter=1;
normw=999;
normp=999;

wold=w;
psyold=psy;
mask=zeros(j_maxn,i_max);
mask(1,:)=1;
mask(j_tmaxn:j_maxn,1:i_tmax)=1;
mask(1:j_nn,i_n:i_n+0.25/dx)=1;
mask(j_maxn,i_tmax:i_max)=1;
mask(:,i_max)=1;
mask(:,1)=1;
mask(2,2:i_n-1)=2;
mask(2,i_n+0.25/dx+1:i_max-1)=2;
mask(j_maxn-1,i_tmax+1:i_max-1)=2;
mask(2:j_tmaxn-1,2)=2;
mask(2:j_maxn-1,i_max-1)=2;
mask(j_tmaxn-1,2:i_tmax)=2;
mask(j_tmaxn:j_maxn-1,i_tmax+1)=2;
mask(2:j_nn,i_n-1)=2;
mask(2:j_nn,i_n+0.25/dx+1)=2;
mask(j_nn+1,i_n:i_n+0.25/dx)=2;

%%  inlet
T(1,1:i_n)=0;
for i=1:i_n-1
    v(1,1:i)=-1*(2*d/(2*d-dx));
end
for i=i_n-1:-1:1
    psy(1,i)=psy(1,i+1)+(v(1,i)+v(1,i+1))*0.5*dx;
end
w(1,i_n)=(2/dx^2)*(psy(1,i_n)-psy(1,i_n-1));

%% nozzle wall
psy(1:j_nn,i_n:i_n+0.25/dx)=psy(1,i_n);
psy(1:j_tmaxn,1)=psy(1,1);
psy(j_tmaxn,1:i_tmax)=psy(1,1);
psy(j_tmaxn:j_maxn,i_tmax)=psy(1,1);
psy(j_maxn,i_tmax:i_max)=psy(1,1);
psy(1,i_n+0.25/dx+1:i_max)=psy(1,i_n+0.25/dx);
%% solution
while (normp>1e-5 )%%|| normw>1e-6)
    %while iter<100
    for j=1:j_maxn
        for i=1:i_max
            if mask(j,i)==1
            else
        psy(j,i)=(bp/(2*(1+beta^2)))*(psy(j,i+1)+psy(j,i-1)+(beta^2)*(psy(j-1,i)+psy(j+1,i))+(dx^2)*w(j,i))+(1-bp)*psy(j,i);
            end
        end
    end
    
    psy(1,i_n+0.25/dx+1:i_max-1)=psy(1,i_n+0.25/dx:i_max-2)+psy(2,i_n+0.25/dx+1:i_max-1)-psy(2,i_n+0.25/dx:i_max-2);
    
    psy(1:j_maxn-1,i_max)=psy(1:j_maxn-1,i_max-1)+psy(2:j_maxn,i_max)-psy(2:j_maxn,i_max-1);
    %psy(1,i_n+0.25/dx+1:i_max)=psy(1,i_n+0.25/dx);
    for j=1:j_maxn
        for i=1:i_max
            if mask(j,i)==1
            else
                u(j,i)=(psy(j-1,i)-psy(j+1,i))/(2*dy);
                v(j,i)=(psy(j,i-1)-psy(j,i+1))/(2*dx);
            end
        end
    end
    v(:,1)=v(:,2);
    u(1,i_n+0.25/dx+1:i_max-1)=u(2,i_n+0.25/dx+1:i_max-1);
    
    v(1,i_n+0.25/dx+1:i_max-1)=v(2,i_n+0.25/dx+1:i_max-1);
    
    u(:,i_max)=u(:,i_max-1);
    v(:,i_max)=v(:,i_max-1);
    Recx(:,:)=u(:,:)*Re*dx;
    Recy(:,:)=v(:,:)*Re*dy;
    
    for j=1:j_maxn
        for i=1:i_max
            if mask(j,i)==1
            else
                if abs(Recx(j,i))<=2
                    Cx=[0.5,0,-0.5]; % central
                    Px=[0 0 0 0 0];
                elseif abs(Recx(j,i))>2
                    if u(j,i)>0
                        Cx=[0,1,-1];
                        Px=[0 3/8 -5/8 1/8 1/8];
                    elseif u(j,i)<0
                        Cx=[1,-1,0];
                        Px=[-1/8 -1/8 5/8 -3/8 0];
                    end
                    
                end
                if abs(Recy(j,i))<=2 %similar for y
                    Cy=[0.5,0,-0.5];
                    Py=[0 0 0 0 0];
                elseif abs(Recy(j,i))>2
                    if v(j,i)>0
                        Cy=[0,1,-1];
                        Py=[0 3/8 -5/8 1/8 1/8];
                    elseif v(j,i)<0
                        Cy=[1,-1,0];
                        Py=[-1/8 -1/8 5/8 -3/8 0];
                    end
                end
                 if mask(j,i)==0
%                      if normp<1e-3 && normw<1e-3
                          C_p=(Px(3)*Recx(j,i)+Py(3)*(beta^2)*Recy(j,i))*w(j,i)+Recx(j,i)*(Px(1)*w(j,i+2)+Px(2)*w(j,i+1)+Px(4)*w(j,i-1)+Px(5)*w(j,i-2))+Recy(j,i)*(beta^2)*(Py(1)*w(j-2,i)+Py(2)*w(j-1,i)+Py(4)*w(j+1,i)+Py(5)*w(j+2,i));
%                      else
%                          C_p=0;
%                     end
                elseif mask(j,i)==2
                    C_p=0;
                end
                Ai=1-Cx(1)*Recx(j,i); %Cx(1)=ax
                Bi=1-Cx(3)*Recx(j,i);%Cx(3)=cx
                Ci=(beta^2)*((Cy(3)*Recy(j,i)-1)*w(j+1,i)+(Cy(1)*Recy(j,i)-1)*w(j-1,i))+C_p-((dx^2)*Re/dt)*wold(j,i);
                Di=-2*(1+(beta^2)+0.5*Cx(2)*Recx(j,i)+0.5*beta^2*Cy(2)*Recy(j,i)+(dx^2)*Re/(2*dt));
                w(j,i)=(bw)*(1/Di)*(-w(j,i+1)*Ai-w(j,i-1)*Bi+Ci)+(1-bw)*w(j,i);
            end
        end
    end
    w(j_tmaxn,1:i_tmax-1)=(-2/dy^2)*(psy(j_tmaxn-1,1:i_tmax-1)-psy(j_tmaxn,1:i_tmax-1));
    w(2:j_nn-1,i_n)=(-2/dx^2)*(psy(2:j_nn-1,i_n-1)-psy(2:j_nn-1,i_n));
    w(j_nn,i_n)=-v(j_nn,i_n-1)/(2*dx)+u(j_nn+1,i_n)/(2*dy);
    if i_n>3
    w(j_nn,i_n+1:i_n+0.25/dx-1)=(-2/dy^2)*(psy(j_nn+1,i_n+1:i_n+0.25/dx-1)-psy(j_nn,i_n+1:i_n+0.25/dx-1));%opt
    end
    w(j_nn,i_n+0.25/dx)=v(j_nn,i_n+0.25/dx+1)/(2*dx)+u(j_nn+1,i_n+0.25/dx)/(2*dy);
    w(j_maxn,i_tmax)=0;
    w(j_tmaxn,i_tmax)=v(j_tmaxn,i_tmax+1)/(2*dx)-u(j_tmaxn-1,i_tmax)/(2*dy);%3
    w(j_tmaxn+1:j_maxn-1,i_tmax)=(-2/dx^2)*(psy(j_tmaxn+1:j_maxn-1,i_tmax+1)-psy(j_tmaxn+1:j_maxn-1,i_tmax));
    w(j_maxn,i_tmax+1:i_max-1)=(-2/dy^2)*(psy(j_maxn-1,i_tmax+1:i_max-1)-psy(1,1));
    w(1:j_nn-1,i_n+0.25/dx)=(-2/dx^2)*(psy(1:j_nn-1,i_n+0.25/dx+1)-psy(1:j_nn-1,i_n+0.25/dx));

    w(:,i_max)=w(:,i_max-1);
    w(1,i_n+0.25/dx+1:i_max-1)=(0.5/dx)*(v(1,i_n+0.25/dx+2:i_max)-v(1,i_n+0.25/dx:i_max-2));
        %w(1,i_n+0.25/dx+1:i_max)=(-2/dy^2)*(psy(2,i_n+0.25/dx+1:i_max)-psy(1,i_n+0.25/dx+1:i_max));

    %normw=normfind(w,wold,1,i_max,1,j_maxn); 
   normp=normfind(psy,psyold,1,i_max,1,j_maxn);
   % V=w-wold;
    %U=psy-psyold;
%     disp(max(max(V)));
%     disp(max(max(U)));
    wold=w; 
    psyold=psy;
    disp(iter);
    iter=iter+1;
    
    %vmod=(u.^2+v.^2).^0.5;
     %contour(x,1-y,psy);
%imagesc(x,y,psy)
    %drawnow
end
%plot(u(17/dy+1:19/dy+1,5/dx+1),1-y(17/dy+1:19/dy+1));

%% Integrated KE Flux
Ekx=zeros(1,i_max);
for i=i_n+0.25/dx:i_max
    if i<=i_tmax
        n_max=j_tmaxn;
    else
        n_max=j_maxn;
    end
    if i==i_n+0.25/dx
            v_field=u(j_nn:n_max,i);
    else
            v_field=u(1:n_max,i);
    end
    Ekx(i)=(dy/6)*(v_field(1)^3+v_field(end)^3+4*sum(v_field(2:2:end-1).^3)+2*(sum(v_field(3:2:end-1).^3)));
end
%% Temp
 
for k=2:4

iter=1;
T=zeros(j_maxn,i_max);
T(j_tmaxn:j_maxn,1:i_tmax)=1;
normt=999;
Told=T;
if k==1
    bt=1.1;
    m=5e-5;
else
    bt=1.1;
    m=1e-5;
end
while normt>m
    for j=2:j_maxn-1
        for i=2:i_max-1
            if mask(j,i)==1
            else
                if abs(Pr(k)*Recx(j,i))<=2
                    Cx=[0.5,0,-0.5]; % central
                    Px=[0 0 0 0 0];
                elseif abs(Pr(k)*Recx(j,i))>2
                    if u(j,i)>0
                        Cx=[0,1,-1];
                        Px=[0 3/8 -5/8 1/8 1/8];
                    elseif u(j,i)<0
                        Cx=[1,-1,0];
                        Px=[-1/8 -1/8 5/8 -3/8 0];
                    end
                    
                end
                if abs(Pr(k)*Recy(j,i))<=2 %similar for y
                    Cy=[0.5,0,-0.5];
                    Py=[0 0 0 0 0];
                elseif abs(Pr(k)*Recy(j,i))>2
                    if v(j,i)>0
                        Cy=[0,1,-1];
                        Py=[0 3/8 -5/8 1/8 1/8];
                    elseif v(j,i)<0
                        Cy=[1,-1,0];
                        Py=[-1/8 -1/8 5/8 -3/8 0];
                    end
                end
                if mask(j,i)==0
                    C_p=(Px(3)*Pr(k)*Recx(j,i)+Py(3)*(beta^2)*Pr(k)*Recy(j,i))*T(j,i)+Pr(k)*Recx(j,i)*(Px(1)*T(j,i+2)+Px(2)*T(j,i+1)+Px(4)*T(j,i-1)+Px(5)*T(j,i-2))+Recy(j,i)*Pr(k)*(beta^2)*(Py(1)*T(j-2,i)+Py(2)*T(j-1,i)+Py(4)*T(j+1,i)+Py(5)*T(j+2,i));
                    %C_p=0;
                elseif mask(j,i)==2
                    C_p=0;
   
                end
                 
                Ai=1-Cx(1)*Pr(k)*Recx(j,i); %Cx(1)=ax
                Bi=1-Cx(3)*Pr(k)*Recx(j,i);%Cx(3)=cx
                Cit=(beta^2)*((Cy(3)*Pr(k)*Recy(j,i)-1)*T(j+1,i)+(Cy(1)*Pr(k)*Recy(j,i)-1)*T(j-1,i))+C_p-((dx^2)*Pr(k)*Re/dt)*T(j,i);
                Di=-2*(1+(beta^2)+0.5*Cx(2)*Pr(k)*Recx(j,i)+0.5*(beta^2)*Cy(2)*Pr(k)*Recy(j,i)+(dx^2)*Pr(k)*Re/(2*dt));
                T(j,i)=(bt/Di)*(-T(j,i+1)*Ai-T(j,i-1)*Bi+Cit)+(1-bt)*T(j,i);
            end
        end
    end
    T(2:j_tmaxn-1,1)=T(2:j_tmaxn-1,2);
    T(2:j_nn,i_n)=T(2:j_nn,i_n-1);
    if i_n>3
    T(j_nn,i_n+1:i_n+0.25/dx-1)=T(j_nn+1,i_n+1:i_n+0.25/dx-1);%opt
    end
    T(1,i_n+0.25/dx+1:i_max-1)=T(2,i_n+0.25/dx+1:i_max-1);
    T(1:j_maxn,i_max)=T(1:j_maxn,i_max-1);
    T(j_maxn,i_tmax+1:i_max-1)=T(j_maxn-1,i_tmax+1:i_max-1);
    T(1:j_nn,i_n+0.25/dx)=T(1:j_nn,i_n+0.25/dx+1);
    %T(j_tmaxn+1:j_maxn,i_tmax)=T(j_tmaxn+1:j_maxn,i_tmax+1);
    normt=normfind(T,Told,1,i_max,1,j_maxn);
    Told=T;
    disp(iter);
    iter=iter+1;

end
Hu=zeros(j_maxn,i_max);
Hv=zeros(j_maxn,i_max);
Tx=zeros(j_maxn,i_max);
Ty=zeros(j_maxn,i_max);
Tx(1:j_tmaxn,1)=0;
Tx(1,2:i_n-1)=(T(1,3:i_n)-T(1,1:i_n-2))/(2*dx);
Tx(1:j_nn,i_n)=0;
Tx(j_nn,i_n:i_n+0.25/dx)=(T(j_nn,i_n+1:i_n+0.25/dx+1)-T(j_nn,i_n-1:i_n+0.25/dx-1))/(2*dx);
Tx(1:j_nn,i_n+0.25/dx)=0;
Tx(1,i_n+0.25/dx+1:i_max-1)=(T(1,i_n+0.25/dx+2:i_max)-T(1,i_n+0.25/dx:i_max-2))/(2*dx);
Tx(:,i_max)=0;
Tx(j_maxn,i_tmax+1:i_max-1)=(T(j_maxn,i_tmax+2:i_max)-T(j_maxn,i_tmax:i_max-2))/(2*dx);
Tx(j_tmaxn+1:j_maxn,i_tmax)=(T(j_tmaxn+1:j_maxn,i_tmax+1)-T(j_tmaxn+1:j_maxn,i_tmax))/(2*dx);
Tx(j_tmaxn,2:i_tmax)=(T(j_tmaxn,3:i_tmax+1)-T(j_tmaxn,1:i_tmax-1))/(2*dx);
Ty(2:j_tmaxn-1,1)=(T(1:j_tmaxn-2,1)-T(3:j_tmaxn,1))/(2*dy);
Ty(1,1:i_max)=(T(1,1:i_max)-T(2,1:i_max))/dy;
Ty(2:j_nn,i_n)=(T(1:j_nn-1,i_n)-T(3:j_nn+1,i_n))/(2*dy);
Ty(2:j_nn,i_n+0.25/dx)=(T(1:j_nn-1,i_n+0.25/dx)-T(3:j_nn+1,i_n+0.25/dx))/(2*dy);
if i_n>3
Ty(j_nn,i_n+1:i_n+0.25/dx-1)=0;%opt
end
Ty(j_tmaxn,1:i_tmax-1)=(T(j_tmaxn-1,1:i_tmax-1)-T(j_tmaxn,1:i_tmax-1))/dy;
Ty(j_maxn,i_tmax:i_max)=0;
Ty(j_tmaxn:j_maxn-1,i_tmax)=(T(j_tmaxn-1:j_maxn-2,i_tmax)-T(j_tmaxn+1:j_maxn,i_tmax))/(2*dy);


    for j=1:j_maxn
        for i=1:i_max
            if mask(j,i)==1
                Hu(j,i)=u(j,i)*T(j,i)-(1/(Pr(k)*Re))*Tx(j,i);
                Hv(j,i)=v(j,i)*T(j,i)-(1/(Pr(k)*Re))*Ty(j,i);
            else
                Hu(j,i)=u(j,i)*T(j,i)-(1/(Pr(k)*Re))*(T(j,i+1)-T(j,i-1))/(2*dx);
                Hv(j,i)=v(j,i)*T(j,i)-(1/(Pr(k)*Re))*(T(j-1,i)-T(j+1,i))/(2*dy);
            end
        end
    end
Jx_in=zeros(1,i_max);
clear sum
for i=i_n+0.25/dx:i_max
    if i<=i_tmax
        n_max=j_tmaxn;
    else
        n_max=j_maxn;
    end
    if i==i_n+0.25/dx
            Hu_field=Hu(j_nn:n_max,i);
    else
            Hu_field=Hu(1:n_max,i);
    end
    Jx_in(i)=(dy/3)*(Hu_field(1)+Hu_field(end)+4*sum(Hu_field(2:2:end-1))+2*(sum(Hu_field(3:2:end-1))));
end
Nu=zeros(1,i_tmax-1);
Nu_side=zeros(1,j_maxn-j_tmaxn+1);
for i=1:i_tmax
    %Nu(i)=-(-3*T(j_tmaxn,i)+4*T(j_tmaxn-1,i)-T(j_tmaxn-2,i))/(2*dy);
   Nu(i)=(T(j_tmaxn,i)-T(j_tmaxn-1,i))/(dy);
end
for i=1:j_maxn-j_tmaxn+1
    Nu_side(i)=(T(i+j_tmaxn-1,i_tmax)-T(i+j_tmaxn-1,i_tmax+1))/(dx);
end
% % average stagnation Nu
% sum=0;
% for i=2:10/dx
%     
%     sum=sum+Nu(i);
% 
%  
% end
% Nu_stag=(dx/0.5)*(sum+Nu(1)*0.5+Nu(0.5/dx+1)*0.5);
% disp(Nu_stag);

ND_Nu=Nu/Nu(1);
if k==1
    T1=T;
    Jx_in1=Jx_in;
    Jx1=Hu;
    Jy1=Hv;
    Nu1=Nu;
    Nu_side1=Nu_side;
    ND_1=ND_Nu;
elseif k==2
    T2=T;
    Jx_in2=Jx_in;
    Jx2=Hu;
    Jy2=Hv;
    Nu2=Nu;
    Nu_side2=Nu_side;
    ND_2=ND_Nu;
elseif k==3
    T3=T;
    Jx_in3=Jx_in;
    Jx3=Hu;
    Jy3=Hv;
    Nu3=Nu;
    Nu_side3=Nu_side;
    ND_3=ND_Nu;
elseif k==4
    T4=T;
    Jx_in4=Jx_in;
    Jx4=Hu;
    Jy4=Hv;
    Nu4=Nu;
    Nu_side4=Nu_side;
    ND_4=ND_Nu;
else
    T5=T;
    Jx_in5=Jx_in;
    Jx5=Hu;
    Jy5=Hv;
    Nu5=Nu;
    Nu_side5=Nu_side;
    ND_5=ND_Nu;
end
            
end
sum1=0;
sum5=0;
for i=2:length(Nu_side1)-1
    
    sum1=sum1+Nu_side1(i);
    sum5=sum5+Nu_side5(i);

end
Nu_HS_1_avg=(dy/1)*(sum1+Nu_side1(1)*0.5+Nu_side1(end)*0.5)
Nu_HS_5_avg=(dy/1)*(sum5+Nu_side5(1)*0.5+Nu_side5(end)*0.5)
sum1=0;
sum5=0;
for i=2:length(Nu1)-1
    
    sum1=sum1+Nu1(i);
    sum5=sum5+Nu5(i);

 
end
Nu_H_1_avg=(dx/10)*(sum1+Nu1(1)*0.5+Nu1(end)*0.5);
Nu_H_5_avg=(dx/10)*(sum5+Nu5(1)*0.5+Nu5(end)*0.5);


% for i=1:j_maxn-j_tmaxn+1
%     Nu_side1(i)=(T1(i+j_tmaxn-1,i_tmax)-T1(i+j_tmaxn-1,i_tmax+1))/(dx);
%     Nu_side2(i)=(T2(i+j_tmaxn-1,i_tmax)-T2(i+j_tmaxn-1,i_tmax+1))/(dx);
%     Nu_side3(i)=(T3(i+j_tmaxn-1,i_tmax)-T3(i+j_tmaxn-1,i_tmax+1))/(dx);
%     Nu_side4(i)=(T4(i+j_tmaxn-1,i_tmax)-T4(i+j_tmaxn-1,i_tmax+1))/(dx);
%     Nu_side5(i)=(T5(i+j_tmaxn-1,i_tmax)-T5(i+j_tmaxn-1,i_tmax+1))/(dx);
% 
%     %Nu_HS_1(i)=-(-3*T1(i+j_tmaxn-1,i_tmax)+4*T1(i+j_tmaxn-1,i_tmax+1)-T1(i+j_tmaxn-1,i_tmax+2))/(2*dx);
%     %Nu_HS_5(i)=-(-3*T5(i+j_tmaxn-1,i_tmax)+4*T5(i+j_tmaxn-1,i_tmax+1)-T5(i+j_tmaxn-1,i_tmax+2))/(2*dx);
% end

% %% Validation
% fid=fopen('TestDataRe400G8.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "X" "Y" U" "V" "T" "Psy"\n');
% fprintf(fid, 'ZONE I=641 J=481  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',x(i));
%         if rem(i,3)==0p
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',20-y(j));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',u(j,i));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',v(j,i));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',T(j,i));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn
%     for i=1:i_max
%         fprintf(fid, '%f ',psy(j,i));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%     end
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('TestU1Re400G8.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U1" "Y" "\n');
% fprintf(fid, 'ZONE I=481 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',u(j,1/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('TestU5Re400G8.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U1" "Y" "\n');
% fprintf(fid, 'ZONE I=481 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',u(j,5/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('TestU9Re400G8.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U9" "Y" "\n');
% fprintf(fid, 'ZONE I=481 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',u(j,9/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('TestNuRe400G8.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "X" "Nu5"  "\n');
% fprintf(fid, 'ZONE I=161 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for i=1:i_tmax
%         fprintf(fid, '%f ',x(i));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% 
% for i=1:i_tmax   
%         fprintf(fid, '%f ',Nu(i)/(20*1.2117));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% 

% %% GCI
% % Pressure solver
% P=zeros(j_maxn,i_max);
% iter=0;
% normPr=999;
% Pold=P;
% bpr=0.1;
% while normPr>1e-5
% for j=1:j_maxn
%         for i=1:i_max
%             if mask(j,i)==1
%             else
%                 S=-(((u(j,i+1)-u(j,i-1))/(2*dx))^2+((v(j-1,i)-v(j+1,i))/(2*dy))^2+2*(u(j-1,i)-u(j+1,i))*(v(j,i+1)-v(j,i-1))/(4*dx*dy));
%                 %S=2*(((psy(j,i+1)-2*psy(j,i)+psy(j,i-1))/(dx^2))*((psy(j+1,i)-2*psy(j,i)+psy(j-1,i))/(dy^2))-(0.5/dx)*((psy(j+1,i+1)-psy(j-1,i+1))/(2*dy)-(psy(j+1,i-1)-psy(j-1,i-1))/(2*dy)));
%             P(j,i)=(-0.25*bpr)*((dx^2)*S-(P(j+1,i)+P(j-1,i)+P(j,i+1)+P(j,i-1)))+(1-bpr)*P(j,i);
%             end
%         end
% end
% P(1,2:i_n-1)=-0.5*v(1,2:i_n-1).^2;
% P(1,i_n)=(1/3)*(4*P(1,i_n-1)-P(1,i_n-2)-(2/Re)*(w(1,i_n)-w(2,i_n)));
% P(2:j_nn-1,i_n)=(1/3)*(4*P(2:j_nn-1,i_n-1)-P(2:j_nn-1,i_n-2)-(1/Re)*(w(1:j_nn-2,i_n)-w(3:j_nn,i_n)));
% P(j_nn,i_n)=(1/3)*(4*P(j_nn,i_n-1)-P(j_nn,i_n-2)-(2/Re)*(-3*w(j_nn,i_n)+4*w(j_nn-1,i_n)-w(j_nn-2,i_n)));
% P(2:j_nn-1,i_n+0.25/dx)=(1/3)*(4*P(2:j_nn-1,i_n+0.25/dx+1)-P(2:j_nn-1,i_n+0.25/dx+2)+(1/Re)*(w(1:j_nn-2,i_n+0.25/dx)-w(3:j_nn,i_n+0.25/dx)));
% P(j_nn,i_n+0.25/dx)=(1/3)*(4*P(j_nn,i_n+0.25/dx+1)-P(j_nn,i_n+0.25/dx+1)+(2/Re)*(-3*w(j_nn,i_n+0.25/dx)+4*w(j_nn-1,i_n+0.25/dx)-w(j_nn-2,i_n+0.25/dx)));
% P(1:j_tmaxn-1,1)=(1/3)*(4*P(1:j_tmaxn-1,2)-P(1:j_tmaxn-1,3));
% P(j_tmaxn,2:i_tmax-1)=(1/3)*(4*P(j_tmaxn-1,2:i_tmax-1)-P(j_tmaxn-2,2:i_tmax-1)-(1/Re)*(w(j_tmaxn,3:i_tmax)-w(j_tmaxn,1:i_tmax-2)));
% P(j_maxn,i_tmax+1:i_max-1)=(1/3)*(4*P(j_maxn-1,i_tmax+1:i_max-1)-P(j_maxn-2,i_tmax+1:i_max-1)-(1/Re)*(w(j_maxn,i_tmax+2:i_max)-w(j_maxn,i_tmax:i_max-2)));
% P(j_tmaxn+1:j_maxn-1,i_tmax)=(1/3)*(4*P(j_tmaxn+1:j_maxn-1,i_tmax+1)-P(j_tmaxn+1:j_maxn-1,i_tmax+2)-(1/Re)*(w(j_tmaxn+2:j_maxn,i_tmax)-w(j_tmaxn:j_maxn-2,i_tmax)));
% P(j_tmaxn,i_tmax)=P(j_tmaxn+1,i_tmax)+P(j_tmaxn,i_tmax-1);
% P(j_maxn,i_tmax)= P(j_maxn,i_tmax+1)+P(j_maxn-1,i_tmax);
% P(j_tmaxn,1)=(1/3)*(4*P(j_tmaxn-1,1)-P(j_tmaxn-2,1)-(1/Re)*(-3*w(j_tmaxn,1)+4*w(j_tmaxn,2)-w(j_tmaxn,3)));
% P(1,i_n+0.25/dx)=(1/3)*(4*P(1,i_n+0.25/dx+1)-P(1,i_n+0.25/dx+2)-(1/Re)*(-3*w(j_nn,i_n+0.25/dx)+4*w(j_nn+1,i_n+0.25/dx)-w(j_nn+2,i_n+0.25/dx)));
% P(1,i_n+0.25/dx+1:i_max-1)=(1/3)*(4*P(2,i_n+0.25/dx+1:i_max-1)-P(3,i_n+0.25/dx+1:i_max-1)-(1/Re)*(w(1,i_n+0.25/dx+2:i_max)-w(1,i_n+0.25/dx:i_max-2))-2*dy*u(1,i_n+0.25/dx+1:i_max-1).*w(1,i_n+0.25/dx+1:i_max-1));
% P(:,i_max)=(1/3)*(4*P(:,i_max-1)-P(:,i_max-2));
% normPr=normfind(P,Pold,1,i_max,1,j_maxn);
% M=P-Pold;
% Pold=P;
% disp(iter);
% iter=iter+1;
%      %contour(x3d,-y3d,P);
%      imagesc(x,y,P);
%      pbaspect([L H 1]);
%      drawnow
% %     colormap('jet');
% end

 %% Post
fid=fopen('Re125Ri0G7Momentum.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "X" "Y" "Psy" "w" "U" "V" "\n');
fprintf(fid, 'ZONE I=481 J=361 K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',x(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',20-y(j));
        
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',psy(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',w(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',u(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',v(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
fclose(fid);
fid=fopen('Re125Ri0G7Energy.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "X" "Y" "Psy" "T1" "T2" "T3" "T4" "T5" "Jx1" "Jx2" "Jx3" "Jx4" "Jx5" "Jy1" "Jy2" "Jy3" "Jy4" "Jy5" "\n');
fprintf(fid, 'ZONE I=481 J=361 K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',x(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',20-y(j));
        
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',psy(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',T1(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',T2(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',T3(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',T4(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',T5(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jx1(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jx2(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jx3(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jx4(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jx5(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jy1(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jy2(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jy3(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jy4(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
for j=1:j_maxn
    for i=1:i_max
        fprintf(fid, '%f ',Jy5(j,i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    end
    fprintf(fid, '\n');
end
fclose(fid);
% % Profiles
% fid=fopen('Re250Ri0G10Uprofile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "U" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',u(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Re250Ri0G10T1profile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',T1(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Re250Ri0G10T2profile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',T2(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Re250Ri0G10T3profile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',T3(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Re250Ri0G10T4profile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',T4(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn   
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
% fid=fopen('Re250Ri0G10T5profile.dat','w+');
% fprintf(fid, 'TITLE = "Example Grid File" \n');
% fprintf(fid, 'FILETYPE = FULL \n');
% fprintf(fid, 'VARIABLES = "T" "Y" "\n');
% fprintf(fid, 'ZONE I=361 J=1  K=1\n');
% fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
% for j=1:j_maxn
%         fprintf(fid, '%f ',T5(j,25/dx+1));
%         
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end
%    
%     fprintf(fid, '\n');
% end
% for j=1:j_maxn    
%         fprintf(fid, '%f ',20-y(j));
%         if rem(i,3)==0
%             fprintf(fid, '\n');
%         end  
%     fprintf(fid, '\n');
% end
% fclose(fid);
fid=fopen('Re125i0G7Nu.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "X" "Nu1" "Nu2" "Nu3" "Nu4" "Nu5" "ND1" "ND2" "ND3" "ND4" "ND5" "\n');
fprintf(fid, 'ZONE I=121 J=1  K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for i=1:i_tmax
        fprintf(fid, '%f ',x(i));
        
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
   
    fprintf(fid, '\n');
end

for i=1:i_tmax   
        fprintf(fid, '%f ',Nu1(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end

for i=1:i_tmax   
        fprintf(fid, '%f ',Nu2(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax   
        fprintf(fid, '%f ',Nu3(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',Nu4(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',Nu5(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',ND_1(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',ND_2(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',ND_3(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',ND_4(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
for i=1:i_tmax    
        fprintf(fid, '%f ',ND_5(i));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
fclose(fid);
fid=fopen('Re125Ri0G7IntegratedFlux.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "X" "Ekx" "Jx_in1" "Jx_in2" "Jx_in3" "Jx_in4" "Jx_in5" "\n');
fprintf(fid, 'ZONE I=472 J=1  K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',x(i));
        
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
   
    fprintf(fid, '\n');
end

for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Ekx(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Jx_in1(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Jx_in2(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Jx_in3(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Jx_in4(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
for i=i_n+0.25/dx:i_max
        fprintf(fid, '%f ',Jx_in5(i));       
        if rem(i,3)==0
            fprintf(fid, '\n');
        end   
    fprintf(fid, '\n');   
end
fclose(fid);

fid=fopen('NuSideWallRe50G1.dat','w+');
fprintf(fid, 'TITLE = "Example Grid File" \n');
fprintf(fid, 'FILETYPE = FULL \n');
fprintf(fid, 'VARIABLES = "NU_HS_1" "NU_HS_2" "NU_HS_3" "NU_HS_4" "NU_HS_5" "Y" "\n');
fprintf(fid, 'ZONE I=19 J=1  K=1\n');
fprintf(fid, 'ZONETYPE=Ordered DATAPACKING=BLOCK\n');
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_side1(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_side2(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_side3(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_side4(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=1:j_maxn-j_tmaxn+1
        fprintf(fid, '%f ',Nu_side5(j));      
        if rem(i,3)==0
            fprintf(fid, '\n');
        end
    fprintf(fid, '\n');
end
for j=j_tmaxn:j_maxn   
        fprintf(fid, '%f ',20-y(j));
        if rem(i,3)==0
            fprintf(fid, '\n');
        end  
    fprintf(fid, '\n');
end
fclose(fid);